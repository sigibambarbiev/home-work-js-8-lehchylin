// 1. Опишіть своїми словами що таке Document Object Model (DOM).
//    Document Object Model (DOM) - это объектная модель документа. Все элементы страницы представляются в виде объектов, свойства которых можно изменять с помощью Java Script.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//    innerText показывает текстовое содержимое строки, в том числе и текст вложенных элементов, т.е. последовательно выведет весь текст, находящийся  внутри отравающихся и закрывающихся тегов.
//    innerHTML покажет весь текст элемента, в том числе и HTML-разметку.

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//    Найти элемент на странице можно несколькими способами, наиболее универсальный - это querySelectorAll - поиск всех элементов, соответствующих заданному селектору.
//    Также достаточно часто используются querySelector (возвращает только первый элемент, соответствующий селектору) и getElementById (поиск по id элемента).
//    Способы getElementsByName, getElementsByTagName, getElementsByClassName и другие используются редко.



// 1.
const paragraf = document.querySelectorAll('p');

for (let elem of paragraf) {
    elem.style.background = '#ff0000';
}

// 2.
const optList = document.getElementById("optionsList");
console.log(optList);

const parentOptList = optList.parentElement;
console.log(parentOptList);


if (!!optList.hasChildNodes()) {
    optList.childNodes.forEach((elem) => console.log(elem.nodeName, elem.nodeType))
}

// 3.
document.getElementById('testParagraph').innerText = 'This is a paragraph';

// 4.
const elemMainHeader = document.querySelector('.main-header');
console.log(elemMainHeader.children);

for (elem of elemMainHeader.children) {
    elem.classList.add('nav-item')
}

// 5.
const elemSectionTitle = document.querySelectorAll('.section-title');

for (elem of elemSectionTitle) {
    elem.classList.remove('section-title')
}
